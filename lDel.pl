#!/usr/bin/perl -w

use strict;

my $bam = shift;
my $targetBed = shift;
my $sam = "/opt/samtools/samtools-0.1.18/samtools";
my $minCov = shift || 1;
my $win = shift || 500;
my $step = shift || 500;
my $winCov = shift || $win / $step; 
my $MAPQ_thresh = shift || 10;
my %target = ();
my %off = ();

my %data = ();
my $cnt = 0;
open P, "$sam depth -Q $MAPQ_thresh -b $targetBed $bam | ";
	while (<P>) {
		my @row = split(/\t/, $_);
		next if ($row[2] < $minCov);
		my $s = int($row[1] / $step);
		foreach my $Wcor (0 .. ($winCov - 1)) {
			$data{$row[0]}{$s - $Wcor}{n}++;
			$data{$row[0]}{$s - $Wcor}{sum} += $row[2];
		}
		print STDERR "\r$cnt $row[0]" if ( ($cnt++ % 100000) == 0  );
	}
close P;

my @chrs = map {"chr$_"} (1 .. 22, "X", "Y", "M");
foreach my $c (@chrs) {
	next unless (exists($data{$c}));
	foreach my $p (sort { $a <=> $b } keys(%{$data{$c}})) {
		my $v = $data{$c}{$p}{sum} / $data{$c}{$p}{n};
		print join("\t", $c, ($step * $p), ($step * $p + $win), $v) . "\n";
	}
}
