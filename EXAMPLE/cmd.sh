#!/bin/bash

../lDel.pl ./BAM/APD001_N1.bam ./BAM/SeqCap_EZ_Exome_v2.bed > P1N.txt
../lDel.pl ./BAM/APD001_T1.bam ./BAM/SeqCap_EZ_Exome_v2.bed > P1T.txt
../lDel.pl ./BAM/APD002_N1.bam ./BAM/SeqCap_EZ_Exome_v2.bed > P2N.txt
../lDel.pl ./BAM/APD002_T1.bam ./BAM/SeqCap_EZ_Exome_v2.bed > P2T.txt
 
